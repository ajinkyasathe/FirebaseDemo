package com.ajinkya.practice.firebasedemo;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity {

    private EditText userName, userPassword, userEmail, userPhone;
    private Button register;
    private TextView backToLogin;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        registrationForm();

        firebaseAuth = FirebaseAuth.getInstance();


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    //This data will go to the database
                    String user_email = userEmail.getText().toString().trim();
                    String user_password = userPassword.getText().toString().trim();
                    //String user_phone = userPhone.getText().toString().trim();
                    firebaseAuth.createUserWithEmailAndPassword(user_email, user_password).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                               // Toast.makeText(Register.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                               // startActivity(new Intent(Register.this, MainActivity.class));
                            sendEmailVerification();
                            } else {
                                Toast.makeText(Register.this, "Registration Unsuccessful", Toast.LENGTH_SHORT).show();
                            }


                        }
                    });
                }
            }
        });

        backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Register.this, MainActivity.class));
            }
        });
    }

    private void registrationForm() {
        userName = (EditText) findViewById(R.id.etRegName);
        userPassword = (EditText) findViewById(R.id.etRegPassword);
        userEmail = (EditText) findViewById(R.id.etRegEmail);
        userPhone = (EditText) findViewById(R.id.etRegPhone);
        register = (Button) findViewById(R.id.btnRegister);
        backToLogin = (TextView) findViewById(R.id.tvBackToLogin);


    }

    private Boolean validate() {
        Boolean result = false;
        String name = userName.getText().toString();
        String password = userPassword.getText().toString();
        String email = userEmail.getText().toString();
        String phone = userPhone.getText().toString();

        if(phone.length() !=10){

            Toast.makeText(this,"Uninvalid Phone No",Toast.LENGTH_SHORT).show();

        }

        if (name.isEmpty() || password.isEmpty() || email.isEmpty() || phone.isEmpty()) {
            Toast.makeText(this, "Please Enter all detail", Toast.LENGTH_SHORT).show();


        } else {
            result = true;
        }
        return result;


    }
    private void sendEmailVerification(){
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if(firebaseUser!=null){
           firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
               @Override
               public void onComplete(@NonNull Task<Void> task) {
                   if(task.isSuccessful()){
                       Toast.makeText(Register.this,"Successfully Registered,Verification Mail Has Been Sent",Toast.LENGTH_SHORT).show();

                       firebaseAuth.signOut();
                       finish();
                       startActivity(new Intent(Register.this,MainActivity.class));

                   }
                   else{
                       Toast.makeText(Register.this,"Verification hasnt been sent",Toast.LENGTH_SHORT).show();
                   }

               }
           });
        }
    }


}
