package com.ajinkya.practice.firebasedemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private EditText Name;
    private EditText Password;
    private Button Login;
    private TextView Registration;
    private int counter = 5;
    private FirebaseAuth auth;
    private ProgressDialog progressDialog;
    private TextView Info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Name = (EditText) findViewById(R.id.etUsername);
        Password = (EditText) findViewById(R.id.etPassword);
        Login = (Button) findViewById(R.id.btnLogin);
        Registration = (TextView) findViewById(R.id.tvRegister);

        Info = (TextView) findViewById(R.id.tvCounter);

        auth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            startActivity(new Intent(MainActivity.this, WarrentykeeperMain.class));
            finish();
        }


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation(Name.getText().toString(), Password.getText().toString());


            }
        });


        Registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Register.class));
            }
        });


    }

    private void validation(String username, String password) {
        progressDialog.setMessage("Hello Welcome to Warrenty Keeper");
        progressDialog.show();


        auth.signInWithEmailAndPassword(username, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    progressDialog.dismiss();
                    // Toast.makeText(MainActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(MainActivity.this, WarrentykeeperMain.class));
                    checkEmailVerification();

                } else {
                    Toast.makeText(MainActivity.this, "Login not successful", Toast.LENGTH_SHORT).show();
                    counter--;
                    Info.setText("No of attempts remaining=" + counter);
                    progressDialog.dismiss();
                    if (counter == 0) {
                        Login.setEnabled(false);
                    }
                }

            }
        });

    }

    private void checkEmailVerification() {

        FirebaseUser firebaseUser = auth.getInstance().getCurrentUser();
        Boolean emailFlag = firebaseUser.isEmailVerified();
        if (emailFlag) {
            finish();
            startActivity(new Intent(MainActivity.this, WarrentykeeperMain.class));
        } else {
            Toast.makeText(this, "Verifty your Email", Toast.LENGTH_SHORT);
            auth.signOut();
        }

    }

}

