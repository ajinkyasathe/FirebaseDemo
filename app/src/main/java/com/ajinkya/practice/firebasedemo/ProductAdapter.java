package com.ajinkya.practice.firebasedemo;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ninzz on 09-03-2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {


    private Context mContext;
    private List<Product> products = new ArrayList<>();

    public ProductAdapter(List<Product> products) {
        this.products.clear();
        this.products.addAll(products);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView productName;
        TextView productType;
        TextView purchaseDate;
        TextView expiryDate;
        ImageView productImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            productName = (TextView) itemView.findViewById(R.id.txt_product_name);
            productType = (TextView) itemView.findViewById(R.id.txt_product_type);
            purchaseDate = (TextView) itemView.findViewById(R.id.txt_purchase_date);
            expiryDate = (TextView) itemView.findViewById(R.id.txt_expiry_date);
            productImage = (ImageView) itemView.findViewById(R.id.img_product);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.product_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        Product product = products.get(position);
        viewHolder.productName.setText(product.getProductName());
        viewHolder.productType.setText(product.getProductType());
        viewHolder.purchaseDate.setText(product.getPurchaseDate());
        viewHolder.expiryDate.setText(product.getExpiryDate());
        Glide.with(mContext).load(product.getImageUri()).into(viewHolder.productImage);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

}
