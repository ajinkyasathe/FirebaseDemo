package com.ajinkya.practice.firebasedemo;

import java.util.Date;

/**
 * Created by ninzz on 09-03-2018.
 */

public class Product {
    private String productName;
    private String productType;
    private String imageUri;
    private String purchaseDate;
    private String expiryDate;


    public Product(String productName, String productType, String purchaseDate, String expiryDate, String imageUri) {
        this.productName = productName;
        this.productType = productType;
        this.imageUri = imageUri;
        this.purchaseDate = purchaseDate;
        this.expiryDate = expiryDate;
    }

    public Product() {

    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }


    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}
