package com.ajinkya.practice.firebasedemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class WarrentykeeperMain extends AppCompatActivity {

    FloatingActionButton add;
    RecyclerView recyclerView;
    List<Product> list;
    ProgressDialog progressDialog;
    Button logout;
    Context mContext;
    ProductAdapter productAdapter;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warrentykeeper_main);
        mContext = WarrentykeeperMain.this;

        firebaseAuth = FirebaseAuth.getInstance();


        add = (FloatingActionButton) findViewById(R.id.actionButton);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_product_list);
        list = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Fetching Data Please Wait");
        progressDialog.show();


        databaseReference = FirebaseDatabase.getInstance().getReference(ProductDetails.Database_path);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                list.clear();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    Product products = snap.getValue(Product.class);
                    list.add(products);
                }
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
//                recyclerView.addItemDecoration(new DividerItemDecoration(mContext,));
                productAdapter = new ProductAdapter(list);
                recyclerView.setAdapter(productAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WarrentykeeperMain.this, ProductDetails.class));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logoutMenu: {
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(WarrentykeeperMain.this, MainActivity.class));

            }
        }
        return super.onOptionsItemSelected(item);
    }
}
