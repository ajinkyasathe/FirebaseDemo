package com.ajinkya.practice.firebasedemo;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

public class ProductDetails extends AppCompatActivity {

    ImageView productImage;
    EditText productName, productType, purchaseDate, expiryDate;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;

    public static final String Storage_path = "images/";
    public static final String Database_path = "mainObject";
    private Uri imageUri;
    Calendar c;
    DatePickerDialog dialog;
    ImageButton datePurchaseButton, dateExpiryButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        productImage = (ImageView) findViewById(R.id.img_product);
        productName = (EditText) findViewById(R.id.edt_product_name);
        productType = (EditText) findViewById(R.id.edt_product_type);
        purchaseDate = (EditText) findViewById(R.id.edt_purchase_date);
        expiryDate = (EditText) findViewById(R.id.edt_expiry_date);
        datePurchaseButton = (ImageButton) findViewById(R.id.btn_purchase_date_picker);
        dateExpiryButton = (ImageButton) findViewById(R.id.btn_expiry_date_picker);
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference(Database_path);

        datePurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DialogFragment date = new DialogFragment();
                // date.show(getSupportFragmentManager(),"Date Picker");

                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);
                dialog = new DatePickerDialog(ProductDetails.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int nyear, int nmonth, int nday) {
                        purchaseDate.setText(nday + "/" + (nmonth + 1) + "/" + nyear);

                    }
                }, day, month, year);
                dialog.show();

            }
        });

        dateExpiryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);
                dialog = new DatePickerDialog(ProductDetails.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int nyear, int nmonth, int nday) {
                        expiryDate.setText(nday + "/" + (nmonth + 1) + "/" + nyear);

                    }
                }, day, month, year);
                dialog.show();


            }
        });
    }


    public void browseImages(View v) {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), 0);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                productImage.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /*public String getActualImage(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }*/

    public void saveData(View v) {

        if (imageUri != null) {
            //insert data
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Saving...");
            progressDialog.show();
            StorageReference reference = storageReference.child(Storage_path + System.currentTimeMillis() + "." + imageUri);
            reference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    String PNAME = productName.getText().toString();
                    String PTYPE = productType.getText().toString();
                    String PURCHASE = purchaseDate.getText().toString();
                    String EXPIRY = expiryDate.getText().toString();

                    Product product = new Product(PNAME, PTYPE, PURCHASE, EXPIRY, taskSnapshot.getDownloadUrl().toString());//,PURCHASE);
                    String id = databaseReference.push().getKey();
                    databaseReference.child(id).setValue(product);
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Product saved successfully", Toast.LENGTH_LONG).show();
                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double totalProgress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded %" + (int) totalProgress);
                        }
                    });
        } else {
            //show message
            Toast.makeText(getApplicationContext(), "Please upload product image", Toast.LENGTH_LONG).show();
        }

    }

    /*public void viewImage(View v) {
        Intent intent = new Intent(ProductDetails.this, WarrentykeeperMain.class);
        startActivity(intent);
    }*/


}
